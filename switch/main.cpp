#include <Arduino.h>
#include <M5Unified.h>

void setup() {
    auto config = M5.config();
    M5.begin(config);

    // 画面の向き  1: ボタンが右
    M5.Lcd.setRotation(1);
    // ディスプレイの明るさ
    M5.Lcd.setBrightness(80);

    // 26番ピンをプルアップ入力モードにする
    pinMode(GPIO_NUM_26, INPUT_PULLUP);
}

static int status = -1;

void loop() {
    // 2つのパーツが近づくとスイッチ ON で GND に接続され 0 に
    // 離れるとプルアップで 3.3 V に
    int val = digitalRead(GPIO_NUM_26);
    if (val != status) {
        status = val;

        M5.Lcd.startWrite();
        M5.Lcd.fillScreen(BLACK);
        M5.Lcd.setTextColor(BLUE);
        M5.Lcd.setTextSize(3); // 3倍
        M5.Lcd.setCursor(0, 0);

        if (status == 0) {
            M5.Lcd.print("CLOSED");
        } else {
            M5.Lcd.print("OPEN");
        }
        M5.Lcd.endWrite();
    }
    delay(100);
}