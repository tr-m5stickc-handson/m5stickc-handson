#include <Arduino.h>
#include <M5Unified.h>
#include <M5_ENV.h>

// オリジナルの Example は以下の URL にあります
// https://github.com/m5stack/M5Unit-ENV/blob/master/examples/Unit_ENVIII_M5StickC/Unit_ENVIII_M5StickC.ino

// 温湿度センサー SHT30 を制御するライブラリ
SHT3X sht30;
// 気圧センサー QMP6988 を制御するライブラリ
QMP6988 qmp6988;

void setup() {
    auto config = M5.config();
    M5.begin(config);

    // ディスプレイの明るさ
    M5.Lcd.setBrightness(0);

    // I2C でセンサーと通信するときに使用するライブラリ
    // 外部コネクタを指定して初期化
    Wire.begin(M5.Ex_I2C.getSDA(), M5.Ex_I2C.getSCL());
    qmp6988.init();
}

void loop() {
    auto ret = sht30.get();
    Serial.println("----------------");
    if (ret == 0) {
        // データが取れた
        Serial.printf("温度: %2.1f ℃\n", sht30.cTemp);
        Serial.printf("湿度: %2.1f %%\n", sht30.humidity);
    } else {
        Serial.println("温度: ---.- ℃");
        Serial.println("湿度: ---.- %");
    }
    Serial.printf("気圧: %4.1f hPa\n", qmp6988.calcPressure() / 100);
    delay(10000);
}