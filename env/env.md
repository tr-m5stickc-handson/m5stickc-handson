気温・気圧センサーの使い方
================================================================================

今回も M5StickC の外部に接続したセンサーを使用します。接続するのは定番の温度、湿度、気圧が測れる環境センサーです。GROVE 互換の4ピンコネクタで接続できるものに、M5 Env III Unit があります。

このユニットの中には、SHT30（温湿度）と QMP6988（気圧）センサーが入っており、I2C というプロトコルで M5StickC と通信して、現在地を取得します。この通信部分を1から実装するのは少し大変です。

今回は、この通信部分で便利なライブラリを使って、簡単に測定値を取得する方法を紹介します。M5StickC には、使用できるライブラリを検索してインストールする便利な機能があります。PlatformIO の Libraries 画面にある検索ボックスに M5Unit-ENV と入れてみて下さい。Add to Project ボタンをクリックすると、追加するプロジェクトを指定して、ライブラリのインストールができます。

![](images/platformio-libraries.png)

ただし、必要なライブラリ名とバージョンがわかっている場合は、platformio.ini を直接編集したほうが早いです。皆さんの platformio.ini を確認して、`lib-deps` に M5Unit-ENV が登録されていなければ、書き足して下さい。

```ini
[env:m5stick-c]
platform = espressif32@^6.0.0
board = m5stick-c
framework = arduino
lib_deps =
	m5stack/M5Unified@^0.1.7
    # 環境センサーのライブラリ
	m5stack/M5Unit-ENV@^0.0.7
monitor_speed = 115200
```

ここで `@^0.0.7` が何を表しているかというと、M5Unit-Env のバージョン 0.0.7 または、互換性のあるより新しいバージョンをインストールすることを表しています。より厳密にバージョンを管理したい場合は、`^` を外して、具体的なバージョンを直接指定することもできます。

では、プログラムに移ります。


```c++
#include <Arduino.h>
#include <M5Unified.h>
#include <M5_ENV.h>

// オリジナルの Example は以下の URL にあります
// https://github.com/m5stack/M5Unit-ENV/blob/master/examples/Unit_ENVIII_M5StickC/Unit_ENVIII_M5StickC.ino

// 温湿度センサー SHT30 を制御するライブラリ
SHT3X sht30;
// 気圧センサー QMP6988 を制御するライブラリ
QMP6988 qmp6988;

void setup() {
    auto config = M5.config();
    M5.begin(config);

    // ディスプレイの明るさ
    M5.Lcd.setBrightness(0);

    // I2C でセンサーと通信するときに使用するライブラリ
    // 外部コネクタを指定して初期化
    Wire.begin(M5.Ex_I2C.getSDA(), M5.Ex_I2C.getSCL());
    qmp6988.init();
}

void loop() {
    auto ret = sht30.get();
    Serial.println("----------------");
    if (ret == 0) {
        // データが取れた
        Serial.printf("温度: %2.1f ℃\n", sht30.cTemp);
        Serial.printf("湿度: %2.1f %%\n", sht30.humidity);
    } else {
        Serial.println("温度: ---.- ℃");
        Serial.println("湿度: ---.- %");
    }
    Serial.printf("気圧: %4.1f hPa\n", qmp6988.calcPressure() / 100);
    delay(10000);
}
```

`SHT3X` と `QMP6988` はそれぞれ SHT30 と QMP6988 を制御するクラスです。M5_ENV.h を `#include` すると使えます。

`Wire.begin()` は I2C の機能を初期化します。引数はどのピンを使用するかで、GROVE 互換コネクタの G32 と G33 を使うように設定しています。

温湿度の取得は `SHT3X` クラスの `get()` 関数で行います。温湿度を測定するには少し時間がかかり、`get()` の戻り値で取得できたかどうかをチェックします。取得した温度は `cTemp` と `humidity` メンバーに入っています。

気圧の方は `calcPressure()` を呼び出すだけで、気圧を取得できます。単位は Pa です。

それでは書き込んで実行してみましょう。このプログラムは取得した値を画面ではなく、シリアルポートに書き出しますので、[シリアルモニター](../serial/serial.md)を表示して下さい。