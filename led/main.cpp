#include <Arduino.h>
#include <M5Unified.h>


void setup() {
    auto config = M5.config();
    M5.begin(config);
    // 画面を消す
    M5.Lcd.setBrightness(0);

    // 10番のピンを出力にする
    pinMode(GPIO_NUM_10, OUTPUT);
}

void loop() {
    digitalWrite(GPIO_NUM_10, 0); // LED 点灯
    delay(1000);
    digitalWrite(GPIO_NUM_10, 1); // LED 消灯
    delay(1000);
}
