M5StickC ハンズオン
================================================================================

このハンズオンは、プログラミングの経験はあるものの、電子工作の経験が少ない人を対象としており、
少し特殊な内容になっています。そのため、開発環境は電子工作でよく使われる Arduino IDE ではなく、
Visual Studio Code と Platform IO を使用し、フル機能の IDE で気持ちよくコーディングができるようにしています。

また、プログラミング言語の説明はほとんどしていません。C/C++ の基本知識が必要です。

一方で、電子工作初心者を対象としていることから、半田付けやブレッドボードは必要ありません。


目次
--------------------------------------------------------------------------------

### 1日目

1. [セットアップ](setup/setup.md)
2. [最初のプログラムと Arduino プログラミングの基本](first-program/first-program.md)
3. [画面に書いてみる](display/display.md)
4. [シリアルモニターの使い方](serial/serial.md)
5. [ボタンを使ってみる](button/button.md)
6. [LED とデジタル出力](led/led.md)

### 2日目

7. [スイッチの接続とデジタル入力](switch/switch.md)
8. [回転角センサーとアナログ入力](adc/adc.md)
9. [気温・気圧センサーの使い方](env/env.md)
10. [サーボモーターの制御](servo/servo.md)


ハンズオンに必要なもの
--------------------------------------------------------------------------------

* M5StickC Plus（[スイッチサイエンス](https://www.switch-science.com/products/6470)）
    * 旧型の M5StickC でも可
* 磁気ドアスイッチセット（[スイッチサイエンス](https://www.switch-science.com/products/7390)）
    * 同じようなものであればなんでもOK
    * 下記のオスオスのジャンパワイヤでも代用可能
* M5Stack用回転角ユニット（[スイッチサイエンス](https://www.switch-science.com/products/6551)）
* M5Stack用温湿度気圧センサユニット Ver.3（ENV Ⅲ）（[スイッチサイエンス](https://www.switch-science.com/products/7254)）
* サーボモーター SG-92R （[秋月](https://akizukidenshi.com/catalog/g/gM-08914/)）または FS90（[スイッチサイエンス](https://www.switch-science.com/products/7111)）など信号互換品
    * 注意: FS90R は連続回転ができるもので、互換性がありません
* ジャンパワイヤ（オスオス）5色10本入り（[スイッチサイエンス](https://www.switch-science.com/products/57)）
    * 同じようなものであれば何でもOKです。実験キットなどを買うと、よく虹色の束になったものが付いてきますが、これを裂いて使っても良いです
    * 3本必要
* USB Type-A から Type-C のケーブル
    * 充電専用でなければ、100均のもので OK
    * 両端が Type-C のケーブルはうまく動作しない可能性あり

### 部品の入手先

通販または秋葉原（など）の店舗で入手して下さい。

* [スイッチサイエンス](https://www.switch-science.com/)
    * 送料が安いので、通販の場合はおすすめ
    * M5 シリーズの正規代理店
* [マルツ](https://www.marutsu.co.jp)
* [秋月電子通商](https://akizukidenshi.com/)
* [千石電商](https://www.sengoku.co.jp/)
