#include <Arduino.h>
#include <M5Unified.h>

static const int CHANNEL = 0;
static const unsigned int PWM_MIN = 102;
static const unsigned int PWM_ZERO_DEG = 297;  // 1.45 [ms] が 0°
static const unsigned int PWM_MAX = 492;

void setup() {
    auto config = M5.config();
    M5.begin(config);

    // 画面の向き
    // 1: ボタンが右
    M5.Lcd.setRotation(1);
    // ディスプレイの明るさ
    M5.Lcd.setBrightness(80);

    pinMode(GPIO_NUM_26, OUTPUT);
    // SG92R は 50 [Hz] の PWM で制御する
    const int freq = 50;
    // 12 [bit] (4096 段階）で PWM の ON の比率を指定
    const int qbit = 12;
    // SG92R で使用できる範囲は
    // 102 (0.5 [ms], -90 [度]) 〜 492 (2.4 [ms], +90 [度])
    ledcSetup(CHANNEL, freq, qbit);
    ledcAttachPin(GPIO_NUM_26, CHANNEL);
}

static unsigned int duty = PWM_ZERO_DEG;
static bool changed = true;

void loop() {
    M5.update();
    if (M5.BtnA.wasPressed() || M5.BtnA.pressedFor(1000)) {
        if (duty < PWM_MAX) {
            duty++;
            changed = true;
        }
    } else if (M5.BtnB.wasPressed() || M5.BtnB.pressedFor(1000)) {
        if (duty > PWM_MIN) {
            duty--;
            changed = true;
        }
    }

    if (changed) {
        ledcWrite(CHANNEL, duty);

        M5.Lcd.startWrite();
        M5.Lcd.fillScreen(BLACK);
        M5.Lcd.setTextColor(BLUE);
        M5.Lcd.setTextFont(7); // 7 セグフォント
        M5.Lcd.setTextSize(1); // 1倍: 48px
        M5.Lcd.setCursor(0, 0);
        M5.Lcd.printf("%04d", duty);
        M5.Lcd.endWrite();
        changed = false;
    }

    delay(16);
}
