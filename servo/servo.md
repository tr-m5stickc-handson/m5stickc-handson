サーボモーターの制御
================================================================================

これまではセンサーを中心に説明し、M5 から外部への出力は画面、シリアルポート、LED くらいでした。やはり、センシングして、その結果に基づいてなにか動作をしてこそ、目的のことを達成できるので、この章では「動き」を実現するもので、取り扱い安いサーボモーターを紹介します。

サーボモータは、名前の通りモータの一種で、マイコン側から、サーボの次の状態を指示し、これに基づいてモータを動かします。このとき、サーボモーターは現在の回転角などの状態を読み取り、自動で指示した状態になるようにモーターを動かします。

今回使用する FS90、SG92R は角度を指定するタイプのサーボモーターで、-90°から 90°の範囲で角度を指定すると、その角度にモータが動きます。これさえあれば、箱を自動で開けたり、電気の物理スイッチを押したりといったことができます。その他にも FS90R のように、回転の向きと速さを指示するものもあります。

安いサーボモーターの場合、角度の指定方法に PWM（パルス幅変調）を使用します。

![](images/pwm.png)

PWM では、図のように決まった周期の ON と OFF を繰り返す信号を使います。50 Hz であれば、周期は 20 ms で、図の上の信号であれば、10 ms の間 ON して、10 ms の間 OFF といったような感じです。下の図では ON の時間が短く、5 ms の間 ON、15 ms の間 OFF の繰り返しです。

今回のサーボの制御では、50 Hz の PWM で、ON の時間の長さで角度を指定します。FS90 や SG92R では、ON の時間と角度は次のように対応しています。もちろん、ONの時間に合わせて、角度は連続的に変化します。

|ONの時間  | 角度|
|---------:|----:|
| 0.50 [ms]|-90°|
| 1.45 [ms]|  0°|
| 2.4  [ms]| 90°|


それでは動かしてみましょう。M5StickC とサーボモーターをオス-オスのジャンパーワイヤで接続します。

![](images/m5-servo-connection.jpg)

| サーボモーター側 | M5StickC |
|------------------|----------|
| オレンジ         | G26      |
| 赤               | 5V➡     |
| 茶色             | GND      |

動きが分かりやすいようにサーボモーターにお好きなアームを付けておいて下さい。


```c++
#include <Arduino.h>
#include <M5Unified.h>

static const int CHANNEL = 0;
static const unsigned int PWM_MIN = 102;
static const unsigned int PWM_ZERO_DEG = 297;  // 1.45 [ms] が 0°
static const unsigned int PWM_MAX = 492;

void setup() {
    auto config = M5.config();
    M5.begin(config);

    // 画面の向き
    // 1: ボタンが右
    M5.Lcd.setRotation(1);
    // ディスプレイの明るさ
    M5.Lcd.setBrightness(80);

    pinMode(GPIO_NUM_26, OUTPUT);
    // SG92R は 50 [Hz] の PWM で制御する
    const int freq = 50;
    // 12 [bit] (4096 段階）で PWM の ON の比率を指定
    const int qbit = 12;
    // SG92R で使用できる範囲は
    // 102 (0.5 [ms], -90 [度]) 〜 492 (2.4 [ms], +90 [度])
    ledcSetup(CHANNEL, freq, qbit);
    ledcAttachPin(GPIO_NUM_26, CHANNEL);
}

static unsigned int duty = PWM_ZERO_DEG;
static bool changed = true;

void loop() {
    M5.update();
    if (M5.BtnA.wasPressed() || M5.BtnA.pressedFor(1000)) {
        if (duty < PWM_MAX) {
            duty++;
            changed = true;
        }
    } else if (M5.BtnB.wasPressed() || M5.BtnB.pressedFor(1000)) {
        if (duty > PWM_MIN) {
            duty--;
            changed = true;
        }
    }

    if (changed) {
        ledcWrite(CHANNEL, duty);

        M5.Lcd.startWrite();
        M5.Lcd.fillScreen(BLACK);
        M5.Lcd.setTextColor(BLUE);
        M5.Lcd.setTextFont(7); // 7 セグフォント
        M5.Lcd.setTextSize(1); // 1倍: 48px
        M5.Lcd.setCursor(0, 0);
        M5.Lcd.printf("%04d", duty);
        M5.Lcd.endWrite();
        changed = false;
    }

    delay(16);
}
```

はじめに PWM の初期化をします。

```c++
ledcSetup(CHANNEL, freq, qbit);
ledcAttachPin(GPIO_NUM_26, CHANNEL);
```

`ledcSetup()` の最初の引数はチャンネルで、最大16の PWM 信号を出力できるようになっています。今回はチャンネル 0 を使用します。2つ目の引数は PWM の周波数です。今回は 50 Hz です。

最後は量子化ビットで、ON の時間の割合を指定するときに、何ビットで 100 を表現するかを指定します。12 bit の場合、4096段階で設定できます。0% は 0、100% は 4096 です。0°は 1.45 ms でした。1.45 ms に相当する値は、この設定の場合、1.45 ms / 20 ms * 4096 ≒ 297 となります。

変な名前の関数ですが、もともと LED の明るさ制御に使うものだったからでしょう。

`ledcAttachPin()` は指定したチャンネルの PWM をどのピンから出力するかを指定します。この場合は G26 です。サーボモーターのオレンジの線に繋がっています。

```c++
ledcWrite(CHANNEL, duty);
```

そして、`ledcWrite()` は ON の時間を実際に設定する関数です。`duty` は 4096 段階での ON の時間の割合です。この関数を呼ぶことで、サーボモーターに取り付けた角度を好きな角度にできます。


それでは、書き込んで動かしてみましょう。書き込みが終わると、サーボモーターが初期位置に動きますので注意して下さい。

ボタンA（大きなボタン）を押すと、`duty` の値が増えて、正方向にサーボモーターが動きます。ボタンB（USB とは反対側の小さなボタン）を押すと `duty` の値が減り、逆方向にサーボモーターが動きます。押し続けることで連続して値を変えることもできます。