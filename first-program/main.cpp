#include <Arduino.h>
#include <M5Unified.h>

void setup() {
    auto config = M5.config();
    M5.begin(config);
}


void loop() {
    M5.Lcd.fillScreen(GREEN);
    delay(1000);
    M5.Lcd.fillScreen(BLUE);
    delay(1000);
}
