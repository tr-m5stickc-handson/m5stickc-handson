画面に描いてみる
================================================================================

M5StickC の特徴は、なんといってもディスプレイがついていることです。パソコン用のアプリと同じ用に絵や文字を描くことができます。
線、円、矩形など、一通り準備されています。

以下のプログラムを見ていただけると、なんとなく分かるのではないかと思います。


```c++
#include <Arduino.h>
#include <M5Unified.h>


void setup() {
    auto config = M5.config();
    M5.begin(config);

    // 画面の向き
    // 1: ボタンが右
    M5.Lcd.setRotation(1);
    // ディスプレイの明るさ
    M5.Lcd.setBrightness(80);
}

static unsigned int counter = 0;

void loop() {
    unsigned int textPosX = 10;
    unsigned int textPosY = (M5.Lcd.height() - 48) / 2;

    unsigned int dotPosX = (M5.Lcd.width() - 20);
    unsigned int dotPosY = M5.Lcd.height() / 2;

    M5.Lcd.startWrite();
    M5.Lcd.fillScreen(BLACK);
    M5.Lcd.setTextColor(WHITE);
    M5.Lcd.setTextFont(7); // 7 セグフォント
    M5.Lcd.setTextSize(1); // 1倍: 48px
    M5.Lcd.setCursor(textPosX, textPosY);
    M5.Lcd.printf("%03d", counter);
    M5.Lcd.endWrite();

    delay(500);

    M5.Lcd.startWrite();
    // 塗りつぶした円を描く
    // 中心の座標、半径、色
    M5.Lcd.fillCircle(dotPosX, dotPosY, 5, WHITE);
    M5.Lcd.endWrite();

    delay(500);
    counter++;
}
```

1回分の絵を描くときは `startWrite()` と `endWrite()` の間に書きます。こうすると少し早くなるそうです。

`fillScreen()` は画面の塗りつぶし、`setTextColor()` は文字色の設定です。フォントはいくつか用意されているので、後述の参考情報をみて下さい。
残念ながら、日本語に対応したフォントは手動で追加しないと使えません。`setTextSize()` は高さではなく、倍率なので注意が必要です。

`fillCircle()` は塗りつぶした円を描く関数です。500 ms の `delay()` の後に円を書き、500 ms 待った後、`fillScreen()` で消してしまうので、白い丸が点滅しているように見えます。

## 参考情報

* 役立つ非公式日本語マニュアル
    * ディスプレイの向き
    * フォント
    * 描画関数の一覧

https://lang-ship.com/reference/unofficial/M5StickC/Tips/M5Display/

こちらの記事も描画例が書いてあって分かりやすいです:   
https://karakuri-musha.com/inside-technology/arduino-m5stickc-04-time-display-for-m5stickc/

このサンプルプログラムでは、M5シリーズの複数のデバイスで同じコードを使用できる M5Unified というライブラリを使用しています。一方、上記のサイトを含む、多くのウェブサイトでは、M5StickC/M5StickCPlus というボードごとに異なるライブラリを使用したコード例のほうが多くあります。M5StickC の場合は、どちらのライブラリも使い方はほとんど同じですので、安心してください。
