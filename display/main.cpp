#include <Arduino.h>
#include <M5Unified.h>


void setup() {
    auto config = M5.config();
    M5.begin(config);

    // 画面の向き
    // 1: ボタンが右
    M5.Lcd.setRotation(1);
    // ディスプレイの明るさ
    M5.Lcd.setBrightness(80);
}

static unsigned int counter = 0;

void loop() {
    unsigned int textPosX = 10;
    unsigned int textPosY = (M5.Lcd.height() - 48) / 2;

    unsigned int dotPosX = (M5.Lcd.width() - 20);
    unsigned int dotPosY = M5.Lcd.height() / 2;

    M5.Lcd.startWrite();
    M5.Lcd.fillScreen(BLACK);
    M5.Lcd.setTextColor(WHITE);
    M5.Lcd.setTextFont(7); // 7 セグフォント
    M5.Lcd.setTextSize(1); // 1倍: 48px
    M5.Lcd.setCursor(textPosX, textPosY);
    M5.Lcd.printf("%03d", counter);
    M5.Lcd.endWrite();

    delay(500);

    M5.Lcd.startWrite();
    // 塗りつぶした円を描く
    // 中心の座標、半径、色
    M5.Lcd.fillCircle(dotPosX, dotPosY, 5, WHITE);
    M5.Lcd.endWrite();

    delay(500);
    counter++;
}