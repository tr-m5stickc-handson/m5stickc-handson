ボタンを使ってみる
================================================================================

M5 と印字された A ボタンを使ってみます。実はボタンのプログラムを0から書こうとすると大変なのですが、簡単にできるように M5StickC のライブラリが用意されています。

プログラムはこんな感じです。

```c++
#include <Arduino.h>
#include <M5Unified.h>


void setup() {
    auto config = M5.config();
    M5.begin(config);

    // 画面の向き
    // 1: ボタンが右
    M5.Lcd.setRotation(1);
    // ディスプレイの明るさ
    M5.Lcd.setBrightness(80);
}

static unsigned int counter = 0;

void loop() {
    M5.update();

    if (M5.BtnA.wasPressed()) {
        counter++;

        unsigned int textPosX = 10;
        unsigned int textPosY = (M5.Lcd.height() - 48) / 2;
        M5.Lcd.startWrite();
        M5.Lcd.fillScreen(BLACK);
        M5.Lcd.setTextColor(WHITE);
        M5.Lcd.setTextFont(7); // 7 セグフォント
        M5.Lcd.setTextSize(1); // 1倍: 48px
        M5.Lcd.setCursor(textPosX, textPosY);
        M5.Lcd.printf("%03d", counter);
        M5.Lcd.endWrite();
    }

    delay(10);
}
```

それでは Build して Upload して、ボタンを何度か押してみて下さい。押した回数のカウントができていますか？

ボタンには他にも関数があります。時間があれば `wasPressed()` を変えて試してみて下さい。

- `wasReleased()`
- `isPressed()`
- `pressedFor(2000)`

その他については、非公式日本語リファレンスをみてみて下さい:   
https://lang-ship.com/reference/unofficial/M5StickC/Class/Button/
