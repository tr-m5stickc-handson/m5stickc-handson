#include <Arduino.h>
#include <M5Unified.h>

void setup() {
    auto config = M5.config();
    M5.begin(config);

    // 画面の向き  1: ボタンが右
    M5.Lcd.setRotation(1);
    // ディスプレイの明るさ
    M5.Lcd.setBrightness(80);

    // G33 をアナログ入力モードにする
    pinMode(GPIO_NUM_33, ANALOG);
}


void loop() {
    // センサー値を読み取る
    // 0〜4095 (12 bit）の分解能でピンの電圧を取得できます
    // 1 はだいたい (3.3 / 4096) [V]
    int input = analogRead(GPIO_NUM_33);

    // 読み取った値の表示
    M5.Lcd.startWrite();
    M5.Lcd.fillScreen(M5.Lcd.color565(input >> 4, input >> 4, input >> 4));
    M5.Lcd.setTextColor(BLUE);
    M5.Lcd.setTextFont(7); // 7 セグフォント
    M5.Lcd.setTextSize(1); // 1倍: 48px
    M5.Lcd.setCursor(0, 0);
    M5.Lcd.printf("%04d", input);
    M5.Lcd.endWrite();

    delay(32);
}
