回転角センサーとアナログ入力
================================================================================

前回は `digitalRead()` を使用して、ピンの電圧の状態を `0` と `1` で取得しました。ピンの電圧はアナログ値で取得することもできます。これは Analog-Digital Convertor（AD変換器）と呼ばれている機能です。

M5StickC の AD 変換では、標準の設定では、現在の電圧を約 3.3 V を基準として、12 bit 4096 段階で読み取ることができます。0 V のときは `0`、約3.3 V のときは `4095` となります。

今回は回転角センサーを使います。4線の GROVE 互換ケーブルで M5StickC と回転角センサーを接続して下さい。この回転角センサーは G33 に繋がっている黄色の先につまみの角度に応じて 0V〜約3.3Vを出力します。電圧を読み取ることで、現在の角度が分かります。

ではプログラムを書いてみましょう。

```c++
#include <Arduino.h>
#include <M5Unified.h>

void setup() {
    auto config = M5.config();
    M5.begin(config);

    // 画面の向き  1: ボタンが右
    M5.Lcd.setRotation(1);
    // ディスプレイの明るさ
    M5.Lcd.setBrightness(80);

    // G33 をアナログ入力モードにする
    pinMode(GPIO_NUM_33, ANALOG);
}


void loop() {
    // センサー値を読み取る
    // 0〜4095 (12 bit）の分解能でピンの電圧を取得できます
    // 1 はだいたい (3.3 / 4096) [V]
    int input = analogRead(GPIO_NUM_33);

    // 読み取った値の表示
    M5.Lcd.startWrite();
    M5.Lcd.fillScreen(M5.Lcd.color565(input >> 4, input >> 4, input >> 4));
    M5.Lcd.setTextColor(BLUE);
    M5.Lcd.setTextFont(7); // 7 セグフォント
    M5.Lcd.setTextSize(1); // 1倍: 48px
    M5.Lcd.setCursor(0, 0);
    M5.Lcd.printf("%04d", input);
    M5.Lcd.endWrite();

    delay(32);
}
```

まず、次のコードで 33 番ピン (G33) をアナログ入力モードにします。

```c++
pinMode(GPIO_NUM_33, ANALOG);
```

現在の値は次のように読み取ることができます。返ってくる値は `0`/`1` ではなく、`0` から `4095` です。

```c++
int input = analogRead(GPIO_NUM_33);
```

AD変換器の設定をもとに、実際の電圧を mV で返す `analogReadMilliVolts()` もありますので、必要に応じて使ってみて下さい。
