シリアルモニターを使ってみる
================================================================================

プログラムを開発していると、現在のセンサーの値などのデバッグ情報を書き出したいことがあります。しかし、M5StickC のディスプレイは小さいので、全部出すのは大変です。

そこで便利なのがシリアルモニターです。M5StickC から文字を USB ケーブルを通して、PC 上のシリアルモニターに書き出すことができます。

準備として、platformio.ini の設定を次のように設定します。`M5.config()` を呼び出したときの、デフォルトのボーレートが 115,200 のためです。

```ini
[env:m5stick-c]
platform = espressif32@^6.0.0
board = m5stick-c
lib_deps =
	m5stack/M5Unified@^0.1.7
framework = arduino
monitor_speed = 115200

# 以下の設定が必要な場合あり
# upload_port = COM3
# monitor_port = COM3
```

プログラムは次の通りです。

```c++
#include <Arduino.h>
#include <M5Unified.h>


void setup() {
    auto config = M5.config();
    M5.begin(config);
    // ディスプレイの明るさ
    M5.Lcd.setBrightness(0);
}

static unsigned int counter = 0;

void loop() {
    Serial.printf("カウンター: %4d\n", counter);
    counter++;
    delay(1000);
}
```

Build して Upload してみましょう。今度はディスプレイにはなにも表示されません。出力はどこへ行ってしまったのでしょうか？

答えはシリアルモニターです。表示して見ましょう。Platform IO のタスクに Serial Monitor がありますので実行してみて下さい。ツールバーの場合🔌のアイコンです。カウンターの出力が出てきました。

![](images/pio-serial-monitor.png)