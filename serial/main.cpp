#include <Arduino.h>
#include <M5Unified.h>


void setup() {
    auto config = M5.config();
    M5.begin(config);
    // ディスプレイの明るさ
    M5.Lcd.setBrightness(0);
}

static unsigned int counter = 0;

void loop() {
    Serial.printf("カウンター: %4d\n", counter);
    counter++;
    delay(1000);
}
